CC = gcc
CFLAGS =-Wall
LIBS =-lm

zadanie2moduly: zadanie2moduly.o mean.o median.o odchylenie.o
$ (CC) $ (CFLAGS) -o zadanie2moduly zadanie2moduly.o mean.o median.o odchylenie.o $(LIBS)
zadanie2moduly.o: zadanie2moduly.c fun.h
$ (CC) $ (CFLAGS) -c zadanie2moduly.c
mean.o: mean.c
$ (CC) $ (CFLAGS) -c mean.c
median.o: median.c
$ (CC) $ (CFLAGS) -c median.c
odchylenie.o: odchylenie.c
$ (CC) $ (CFLAGS) -c odchylenie.c