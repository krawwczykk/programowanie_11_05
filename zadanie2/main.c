#include <stdlib.h>
#include <stdio.h>
#include "fun.h"
#include <string.h>
#define n 50

struct dane
{
    float X[n];
    float Y[n];
    float RHO[n];
};

struct stat
{
    float Mean[2];
    float Median[2];
    float Standard_deviation[2];
};

int main()
{

    struct dane dane_1;
    struct stat stat_1;

    FILE *plik;

    if ((plik = fopen("P0001_attr.rec.txt", "r")) == NULL)

    {
        printf("Nie mozna otworzyc pliku. ");
    }
    else
    {
        fscanf(plik, "%*[^\n]\n");
        int i = 0;

        while (fscanf(plik, "%*f\t%f\t%f\t%f\n", &dane_1.X[i], &dane_1.Y[i], &dane_1.RHO[i]) == 3)
        {
            printf("Tablica X: %f\n ", dane_1.X[i]);
            printf("Tablica Y: %f\n ", dane_1.Y[i]);
            printf("Tablica RHO: %f\n ", dane_1.RHO[i]);

            i++;
        }
    }
    fclose(plik);

    float sredniaX = mean(n, dane_1.X);
    float sredniaY = mean(n, dane_1.Y);
    float sredniaRHO = mean(n, dane_1.RHO);

    float medianaX = median(n, dane_1.X);
    float medianaY = median(n, dane_1.Y);
    float medianaRHO = median(n, dane_1.RHO);

    float odchylenieX = odchylenie(n, dane_1.X, sredniaX);
    float odchylenieY = odchylenie(n, dane_1.Y, sredniaY);
    float odchylenieRHO = odchylenie(n, dane_1.RHO, sredniaRHO);

    stat_1.Mean[0] = sredniaX;
    stat_1.Median[0] = medianaX;
    stat_1.Standard_deviation[0] = odchylenieX;
    stat_1.Mean[1] = sredniaY;
    stat_1.Median[1] = medianaY;
    stat_1.Standard_deviation[1] = odchylenieY;
    stat_1.Mean[2] = sredniaRHO;
    stat_1.Median[2] = medianaRHO;
    stat_1.Standard_deviation[2] = odchylenieRHO;

    for (int i = 0; i < 3; i++)
    {
        printf("Tablica  %d to: %f\t%f\t%f\n", i, stat_1.Mean[i], stat_1.Median[i], stat_1.Standard_deviation[i]);
    }

    if ((plik = fopen("P0001_attr.rec.txt", "a+")) == NULL)
        printf("Blad otwarcia pliku");
    else
    {
        char *p;
        char *c = "~~~~";
        rewind(plik);
        int finded = 0;
        char linia[10000] = "";
        while (fgets(linia, 10000, plik))
        {
            p = strstr(linia, c);
            if (p != NULL)
            {
                finded = 1;
                break;
            }
        }
        if (finded == 0)
        {
            fprintf(plik, "\n\n\n ");
            fprintf(plik, "Srednia :\n dla X wynosi: %f \n dla Y wynosi:%f \ndla RHO wynosi:%f\n ", sredniaX, sredniaY, sredniaRHO);
            fprintf(plik, "Mediana  :\n dla X wynosi: %f \n dla Y wynosi:%f \ndla RHO wynosi:%f", medianaX, medianaY, medianaRHO);
            fprintf(plik, "Odchylenie Standardowe :\n dla X wynosi: %f \n dla Y wynosi:%f \ndla RHO wynosi:%f\n", odchylenieX, odchylenieY, odchylenieRHO);
            fprintf(plik, "\n\n\n ");
        }
    }
    fclose(plik);
    return 0;
}