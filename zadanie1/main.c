#include <stdlib.h>
#include <stdio.h>
#include "fun.h"

int main()
{
    int n = 50;

    FILE *plik;

    float *array_X;
    array_X = (float *)malloc(n * sizeof(float));
    float *array_Y;
    array_Y = (float *)malloc(n * sizeof(float));
    float *array_RHO;
    array_RHO = (float *)malloc(n * sizeof(float));

    if ((plik = fopen("P0001_attr.rec.txt", "r")) == NULL)

    {
        printf("Nie mozna otworzyc pliku. ");
    }
    else
    {

        fscanf(plik, "%*[^\n]\n");
        int i = 1;

        while (fscanf(plik, "%*f\t%f\t%f\t%f\n", &array_X[i], &array_Y[i], &array_RHO[i]) == 3)
        {

            i++;
        }
    }
    fclose(plik);

    float sredniaX = mean(n, array_X);
    float sredniaY = mean(n, array_Y);
    float sredniaRHO = mean(n, array_RHO);

    printf("\nWartosc sredniej X: %f", sredniaX);
    printf("\nWartosc sredniej Y: %f", sredniaY);
    printf("\nWartosc sredniej RHO: %f", sredniaRHO);

    float medianaX = median(n, array_X);
    float medianaY = median(n, array_Y);
    float medianaRHO = median(n, array_RHO);

    printf("\nMediana X wynosi: %f", medianaX);
    printf("\nMediana Y wynosi: %f", medianaY);
    printf("\nMediana RHO wynosi: %f", medianaRHO);

    float odchylenieX = odchylenie(n, array_X, sredniaX);
    float odchylenieY = odchylenie(n, array_Y, sredniaY);
    float odchylenieRHO = odchylenie(n, array_RHO, sredniaRHO);

    printf("\nWartosc odchhylenia standardowego wynosi: %f", odchylenieX);
    printf("\nWartosc odchhylenia standardowego wynosi: %f", odchylenieY);
    printf("\nWartosc odchhylenia standardowego wynosi: %f", odchylenieRHO);

    if ((plik = fopen("P0001_attr.rec.txt", "a")) == NULL)

    {
        printf("Nie mozna otworzyc pliku. ");
    }
    else
    {

        fprintf(plik, "\n\n");
        fprintf(plik, " SREDNIA DLA: \nX wynosi: %f \nY wynosi: %f \nRHO wynosi: %f\n", sredniaX, sredniaY, sredniaRHO);
        fprintf(plik, " MEDIANA DLA: \nX wynosi: %f \nY wynosi: %f \nRHO wynosi: %f\n", medianaX, medianaY, medianaRHO);
        fprintf(plik, " ODCHYLENIE STANDARDOWE DLA: \nX wynosi: %f \nY wynosi: %f \nRHO wynosi: %f\n", odchylenieX, odchylenieY, odchylenieRHO);
    }

    fclose(plik);

    free(array_X);
    free(array_Y);
    free(array_RHO);

    return 0;
}